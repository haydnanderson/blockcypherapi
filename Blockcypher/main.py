from urllib.request import urlopen, Request
import requests
import json

class Blockcypher:

    supported_coins = {
        "BTC": [
            "main",
            "test3"
        ],
        "DASH": [
            "main"
        ],
        "DOGE": [
            "main"
        ],
        "LTC": [
            "main"
        ]
    }
    endpoints = {

      "txs": {
        "base": {
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "GET",
          "data": "$TXHASH"
        },
        "push":{
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "POST",
          "data": "$RAWTX"
        },
        "confidence": {
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "GET",
          "data": "$TXHASH",
          "end_cap": "/confidence"
        }
      },
      "addrs": {
        "base": {
          "supported_coins": ['BTC', "BTCTEST", "ETH"],
          "type": "GET",
          "data": "ADDRESS"
        },
        "balance": {
          "supported_coins": ['BTC', "BTCTEST", "ETH"],
          "type": "GET",
          "data": "$ADDRESS",
          "end_cap": "/balance"
        },
        "full": {
          "supported_coins": ['BTC', "BTCTEST"],
          "type": "GET",
          "data": "$ADDRESS",
          "end_cap": "/full"
        }
      }
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }

    def __init__(self, api_key=None, proxy=None, coin="BTC", subset="main"):
        self.api_key = api_key
        if(proxy != None):
            self.proxy = {"http":proxy, "https":proxy}
        else:
            self.proxy = proxy

        if(coin in self.supported_coins):
            if(subset in self.supported_coins[coin]):
                self.coin = coin.lower()
                self.subset = subset.lower()
            else:
                # TODO: Assert error
                print("Fail")
        else:
            # TODO: Assert error here
            print("Fail")
        self.base_url = "https://api.blockcypher.com/v1/{coin}/{chain}/".format(coin=self.coin, chain=self.subset)

    def get_valid_json(self,request, allow_204=False):
        if request.status_code == 429:
            raise RateLimitError('Status Code 429', request.text)
        elif request.status_code == 204 and allow_204:
            return True
        try:
            return request.json()
        except JSONError as error:
            msg = 'JSON deserialization failed: {}'.format(str(error))
        raise requests.exceptions.ContentDecodingError(msg)

    def build_url(self, *args, data=None):
        """
        Pass the variables in as an argument
        param data: for post requests
        """
        args, endpoint, subpoint = args[:-2][0],args[-2],args[-1]
        if(len(args) > 1):
            param = ";".join(str(e) for e in args)
        else:
            param = args[0]

        if(endpoint in self.endpoints):
            if(subpoint in self.endpoints[endpoint]):
                # Code to check if coin is supported
                if(self.endpoints[endpoint][subpoint]['type'] == "POST"):
                    return("{endpoint}/{subpoint}".format(endpoint=endpoint, subpoint=subpoint))
                if("end_cap" in self.endpoints[endpoint][subpoint]):
                    end_cap = self.endpoints[endpoint][subpoint]['end_cap']
                    return("{endpoint}/{param}{endcap}".format(endpoint=endpoint, param=param, endcap=end_cap))
                else:
                    return("{endpoint}/{param}".format(endpoint=endpoint, param=param))

    def base_endpoint(self, url, data=None, post=False):
        if(not post):
            req = requests.get(
                url,
                data=None,
                headers=self.headers,
                proxies=self.proxy
            )

        else:
            req = requests.get(
                url,
                data=None,
                headers=self.headers,
                proxies=self.proxy
            )

        data = self.get_valid_json(req)
        return(data)

    def get_tx(self, *args, include_balance=True):
        """
        Pass the addresses in as args for transaction retrieval
        """
        response = dict((address, {"final_balance":None,"txrefs":{},"unconfirmed_txrefs":{}}) for address in args)

        url = self.base_url+self.build_url(args, "addrs", "base")
        print(url)
        r = self.base_endpoint(url)

        # If more than one address provided in arguments
        if(len(args) > 1):
            for i in r:
                if(include_balance):
                    response[i['address']]['final_balance'] = i['final_balance']
                if("txrefs" in i):
                    response[i['address']]['txrefs'] = i['txrefs']
                if("unconfirmed_txrefs" in i):
                    response[i['address']]['unconfirmed_txrefs'] = i['unconfirmed_txrefs']
        else:
            if(include_balance):
                response[r['address']]['final_balance'] = r['final_balance']
            if("txrefs" in r):
                response[r['address']]['txrefs'] = r['txrefs']
            if("unconfirmed_txrefs" in r):
                response[r['address']]['unconfirmed_txrefs'] = r['unconfirmed_txrefs']

        return response

    def push_tx(self, tx):
        data = {
            "tx": tx
        }

        url = self.base_url+self.build_url(tx, "txs", "push")
        r = self.base_endpoint(url, post=True, data=data)
        return(r)

    def get_address_balance(self, *args):
        url = self.base_url+self.build_url(args, "addrs", "balance")
        r = self.base_endpoint(url)
        return r
